FROM php:8.2-fpm

RUN apt-get update && apt-get install -y \
    sudo \
    libpq-dev \
    libonig-dev \
    build-essential \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libwebp-dev libjpeg62-turbo-dev libpng-dev libxpm-dev \
    libfreetype6 \
    libfreetype6-dev \
    locales \
    zip \
    nano \
    mc \
    jpegoptim optipng pngquant gifsicle \
    unzip \
    git \
    curl

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install \
    mbstring \
    exif \
    pcntl \
    bcmath \
    gd \
    pdo_pgsql

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

COPY --chown=www:www entrypoint.sh /tmp/entrypoint.sh

WORKDIR /home/www/install
RUN chown www:www /home/www/install
USER www:www
RUN git clone https://access_token:$GITLAB_TOKEN@gitlab.com/Po4e4ka/my_delivery_city.git .
RUN composer install

USER root
EXPOSE 8000
CMD ["bash", "/tmp/entrypoint.sh"]