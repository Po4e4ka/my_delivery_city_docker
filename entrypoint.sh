if [ ! -d "/home/www/project/.git" ]; then
  chown -R www:www /home/www/project /home/www/install
  mv /home/www/install/* /home/www/project/
  mv /home/www/install/.* /home/www/project/
fi
sudo -u www php artisan serve
#sudo -u www php-fpm